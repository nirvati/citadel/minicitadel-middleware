import process from 'node:process';
import * as path from 'node:path';
import * as fs from 'node:fs';
import * as dns from 'node:dns';
import ILightningClient from './lightning/abstract.js';
import LNDService from './lightning/lnd.js';

const LND_HOST = process.env.LND_HOST ?? '127.0.0.1';
const LND_DIR = process.env.LND_DIR ?? '/lnd';
const LND_PORT = process.env.LND_PORT ?? 10009;
const LND_NETWORK = process.env.LND_NETWORK ?? 'mainnet';

const TLS_FILE = path.join(LND_DIR, 'tls.cert');
const MACAROON_FILE = path.join(
  LND_DIR,
  'data',
  'chain',
  'bitcoin',
  LND_NETWORK,
  'admin.macaroon',
);

export default async function getLightning(): Promise<ILightningClient> {
  let target = LND_HOST;
  if (target.endsWith('.local')) {
    try {
      const ip = await new Promise<string>((resolve, reject) => dns.lookup(target, 4, (err, address) => {
        if (err) reject(err);
        else resolve(address);
      })); 
      target = ip;
    } catch {}
  }
    
  switch (process.env.LIGHTNING_IMPL) {
    case 'lnd':
    case undefined:
      return new LNDService(
        `https://${target}:${LND_PORT}/`,
        fs.readFileSync(TLS_FILE),
        MACAROON_FILE,
      );
    default:
      throw new Error(
        `Unknown lightning implementation: ${process.env.LIGHTNING_IMPL}`,
      );
  }
}
