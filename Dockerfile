FROM oven/bun:latest

WORKDIR /app
COPY package.json bun.lockb ./

RUN bun install --production --frozen-lockfile

COPY . .

# Command should be defined in docker-compose.yml
